### development

```shell
$ brew install jq
$ pip3 install virtualenv
$ python3 -m virtualenv venv
$ source venv/bin/activate
$ pip install -r requirement.py
```

### execute stack

```shell
$ ./cfn_run.sh blog_gcchaan_com.py
# make sure change domain name of distribution to s3 web domain.
```

### TODO

- OAI
- Add test & CI
  - use moto
