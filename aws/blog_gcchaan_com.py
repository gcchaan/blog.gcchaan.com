from boto3.session import Session
from troposphere import GetAtt, Join, Ref
from troposphere import Output, Parameter, Template
from troposphere.cloudfront import Distribution
from troposphere.cloudfront import DistributionConfig
from troposphere.cloudfront import Origin, DefaultCacheBehavior
from troposphere.cloudfront import ForwardedValues, S3Origin, ViewerCertificate
from troposphere.route53 import RecordSetType, AliasTarget
from troposphere.s3 import Bucket, WebsiteConfiguration, BucketPolicy

DOMAIN = 'gcchaan.com'
SUB_DOMAIN = 'blog.' + DOMAIN
S3Bucket = 'GcchaanBucketForWeb'
BlogGcchaanDistribution = 'BlogGcchaanDistribution'

t = Template()

t.add_description(SUB_DOMAIN)

session = Session(region_name='us-east-1')
acm = session.client('acm')
acm_arn = acm.list_certificates(
    )['CertificateSummaryList'][0]['CertificateArn']

hostedzone = t.add_parameter(Parameter(
    'HostedZone',
    Description='The DNS name of an existing Amazon Route 53 hosted zone',
    Type='String',
    Default=DOMAIN,
))

web = t.add_resource(Bucket(
    S3Bucket,
    BucketName=SUB_DOMAIN,
    AccessControl="PublicRead",
    WebsiteConfiguration=WebsiteConfiguration(
        IndexDocument='index.html',
        ErrorDocument='404.html')
))

t.add_resource(BucketPolicy(
    'BucketPolicy',
    PolicyDocument={
        "Version": '2012-10-17',
        "Statement": [{
            "Effect": 'Allow',
            "Principal": '*',
            "Action": 's3:GetObject',
            "Resource": 'arn:aws:s3:::' + SUB_DOMAIN + '/*'
        }]
    },
    Bucket=Ref(S3Bucket),
))

t.add_resource(Distribution(
    BlogGcchaanDistribution,
    DistributionConfig=DistributionConfig(
        Aliases=[SUB_DOMAIN],
        Origins=[Origin(
            Id="Origin 1",
            DomainName=GetAtt(S3Bucket, 'DomainName'),
            S3OriginConfig=S3Origin())],
        DefaultCacheBehavior=DefaultCacheBehavior(
            TargetOriginId='Origin 1',
            ForwardedValues=ForwardedValues(
                QueryString=False,
            ),
            ViewerProtocolPolicy='redirect-to-https'
        ),
        Enabled=True,
        PriceClass='PriceClass_200',
        ViewerCertificate=ViewerCertificate(
            AcmCertificateArn=acm_arn,
            SslSupportMethod='sni-only',
        ),
        HttpVersion='http2',
        DefaultRootObject='index.html'
    )
))


t.add_resource(RecordSetType(
    'WebsiteDNSName',
    AliasTarget=AliasTarget(
        HostedZoneId='Z2FDTNDATAQYW2',
        DNSName=GetAtt(BlogGcchaanDistribution, 'DomainName')
    ),
    HostedZoneName=Join('', [Ref(hostedzone), '.']),
    Comment='DNS name for CloudFront.',
    Name=Join('', [SUB_DOMAIN, '.']),
    Type='A',
))

t.add_output(Output(
    'DistributionID',
    Description='DistributionID',
    Value=Ref(BlogGcchaanDistribution)
))

print(t.to_json())
