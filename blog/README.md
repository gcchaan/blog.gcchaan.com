### development

```shell
$ brew install hugo

# add content
$ hugo new [prefix/]hoge.md

# server
$ hugo server -t vienna -wD

# bild
$ hugo -t vienna

# deploy
$ aws s3 sync ./public s3://blog.gcchaan.com
```

### TODO

- Add CI